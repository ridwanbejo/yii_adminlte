<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminLteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'adminlte/dist/css/AdminLTE.min.css',
        'adminlte/dist/css/skins/_all-skins.min.css',
    ];

    public $js = [
        'adminlte/bootstrap/js/bootstrap.min.js',            
        'adminlte/plugins/slimScroll/jquery.slimscroll.min.js',            
        'adminlte/plugins/fastclick/fastclick.min.js',            
        'adminlte/dist/js/app.min.js',            
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
