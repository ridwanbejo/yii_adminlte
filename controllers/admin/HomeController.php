<?php

namespace app\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class HomeController extends Controller
{
	public $layout = 'adminlte';

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionSampleForm()
	{
		return $this->render('sample_form');
	}
}